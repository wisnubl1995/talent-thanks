import firebase from 'firebase/app'
import 'firebase/database'
import 'firebase/auth'
import 'firebase/firestore'
import firebaseConfig from './firebaseconfig'


const firebaseApp = firebase.initializeApp(firebaseConfig)

export default firebaseApp.firestore()