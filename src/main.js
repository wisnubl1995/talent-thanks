import Vue from 'vue'
import App from './App.vue'
import router from './router'
import firebase from "firebase/app"
import "firebase/auth"

import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap'
import '@/assets/css/main.css'
import store from './store'
import VueScreen from 'vue-screen'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/css/swiper.css'
import VueDragscroll from 'vue-dragscroll'
import VueAnalytics from 'vue-analytics'
import VueApexCharts from 'vue-apexcharts'
Vue.use(VueAnalytics, {
    id: 'UA-194347984-2',
    checkDuplicatedScript: true,
    router
})

Vue.use(VueApexCharts)
Vue.use(VueDragscroll)
Vue.use(VueAwesomeSwiper)
Vue.use(VueScreen)
Vue.config.productionTip = false


new Vue({
    render: h => h(App),
    store,
    router
}).$mount('#app')

// let app
// firebase.auth().onAuthStateChanged(user => {
//   if (!app) {
//     app = new Vue({
//       router,
//       store,
//       render: h => h(App)
//     }).$mount('#app')
//   }
// });