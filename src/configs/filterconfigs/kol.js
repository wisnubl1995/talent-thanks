export const users = [{
        id: 1,
        name: 'Alexandria',
        gender: 'Female',
        platform: ['TikTok', 'Instagram'],
        city: 'Manado',
        field: 'music'
    },
    {
        id: 2,
        name: 'Alexander',
        gender: 'Male',
        platform: ['TikTok'],
        city: 'Jakarta',
        field: 'parenting'
    },
    {
        id: 3,
        name: 'Jane',
        gender: 'Female',
        platform: ['TikTok', 'Instagram'],
        city: 'Manado',
        field: 'music'
    },
    {
        id: 4,
        name: 'Eugenia',
        gender: 'Female',
        platform: ['TikTok'],
        city: 'Jakarta',
        field: 'parenting'
    },
    {
        id: 5,
        name: 'Egy',
        gender: 'Male',
        platform: ['TikTok'],
        city: 'Jakarta',
        field: 'business'
    },
]