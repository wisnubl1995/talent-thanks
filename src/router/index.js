import Vue from 'vue'
import firebase from 'firebase/app'
import "firebase/auth"
import Router from 'vue-router'
import Home from '@/components/Home'
// import Model from '@/components/Model'
import VoiceOver from '../components/PagesInner/VoiceOver'
import Model from "../components/PagesInner/Model"
import Career from "../components/PagesInner/Career"
import Affirmative from "../components/bcamp/Affirmative"
// Career
import InitialFragment from "../components/PagesInner/Career/InitialFragment"
import ModelApply from "../components/PagesInner/Career/ModelApply"
import KeyOpinionLeaderApply from "../components/PagesInner/Career/KeyOpinionLeaderApply"
import VoiceOverApply from "../components/PagesInner/Career/VoiceOverApply"
import KeyOpinionLeader from "../components/PagesInner/KeyOpinionLeader.vue"
import About from "../components/PagesInner/About.vue"
import TestingCode from "../components/TestingCode.vue"
Vue.use(Router);
const auth = firebase.auth();
const routes = [

    {
        path: '/Model/:Mid',
        name: 'model',
        component: Model,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/VoiceOver/:Vid',
        name: 'voice',
        component: VoiceOver,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/',
        name: 'home',
        component: Home,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/KeyOpinionLeader/:Kid',
        name: 'kol',
        component: KeyOpinionLeader,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/Career",
        name: "career",
        component: Career,
        meta: {
            requiresAuth: true
        },
        children: [{
                path: '',
                component: InitialFragment,
                name: "career"
            },
            {
                path: '/ModelApply',
                component: ModelApply,
                name: "Model Application"
            }, {
                path: '/VoiceOverApply',
                component: VoiceOverApply,
                name: "VO Application"
            },
            {
                path: '/KeyOpinionLeaderApply',
                component: KeyOpinionLeaderApply,
                name: "KOL Application"
            },
            {
                path: '/testing',
                component: TestingCode,
                name: "testing"
            },
        ]
    },
    {
        path: "/About",
        name: "about",
        component: About
    },
    {
        path: "/affirmative",
        name: "affirmative",
        component: Affirmative,
    }
]



const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
})

router.beforeEach((to, from, next) => {
        window.scrollTo(0, 0);
        next();
    })
    // below line implement later

// router.beforeEach((to, from, next) => {
//     const requiresAuthTo = to.matched.some(x => x.meta.requiresAuth);
//     // const requiresAuthFrom = from.matched.some(x=> x.meta.requiresAuth);
//     // in here we require auth, if logged in,  requires type, and logs the user
//     if (requiresAuthTo && !auth.currentUser) {
//         next('/')
//     } else {
//         next()
//     }
// })

export default router