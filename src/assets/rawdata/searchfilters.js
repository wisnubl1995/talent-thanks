// KOL AGES
export const KOLAges = {
    filterTitle: "Leader Age:",
    Deeps: [
        { displayed: "All", type: "number", min_val: null, max_val: null },
        { displayed: "18-20", type: "number", min_val: 18, max_val: 20 },
        { displayed: "20-30", type: "number", min_val: 20, max_val: 30 },
        { displayed: "30-40", type: "number", min_val: 30, max_val: 40 },
        { displayed: "40-50", type: "number", min_val: 40, max_val: 50 },
        {
            displayed: "50-above",
            type: "number",
            min_val: 50,
            max_val: 99999999999,
        },
    ],
};
export const KOLGender = {
    filterTitle: "Leader Gender:",
    key: "gender",
    placement: 1,
    Deeps: [
        { displayed: "Male", type: "string", val: "Male" },
        { displayed: "Female", type: "string", val: "Female" },
    ],
};
export const KOLCity = {
    filterTitle: "City:",
    key: "city",
    placement: 2,
    Deeps: [
        { displayed: "Jakarta", type: "string", val: "Jakarta" },
        { displayed: "Manado", type: "string", val: "Manado" },
    ],
};
export const KOLField = {
    filterTitle: "Field:",
    key: "field",
    placement: 3,
    Deeps: [
        { displayed: "parenting", type: "string", val: "parenting" },
        { displayed: "health", type: "string", val: "health" },
        { displayed: "food", type: "string", val: "food" },
        { displayed: "gaming", type: "string", val: "gaming" },
        { displayed: "fashion", type: "string", val: "fashion" },
        { displayed: "technology", type: "string", val: "technology" },
        { displayed: "business", type: "string", val: "business" },
        { displayed: "music", type: "string", val: "music" },
        { displayed: "pet", type: "string", val: "pet" },
    ],
};
export const KOLPlatform = {
    filterTitle: "Platform:",
    key: "platform",
    placement: 4,
    Deeps: [
        { displayed: "TikTok", type: "string", val: "TikTok" },
        { displayed: "Instagram", type: "string", val: "Instagram" },
    ],
};
export const AudienceAmount = {
    filterTitle: "Followers Amount:",
    key: "audience",
    placement: 5,
    Deeps: [
        { displayed: "All", type: "number", min_val: null, max_val: null },
        {
            displayed: "1000-2000",
            type: "number",
            min_val: 1000,
            max_val: 2000,
        },
        {
            displayed: "2000-3000",
            type: "number",
            min_val: 2000,
            max_val: 3000,
        },
        {
            displayed: "3000-Above",
            type: "number",
            min_val: 3000,
            max_val: 99999999999,
        },
    ],
};