export default {
    namespaced: true,
    state: {
      currentRoute: ""
    },
    getters: {
      currentRoute: s => s.currentRoute,
    },
    mutations: {
      setCurrentRoute(s, { y }) {
        s.currentRoute = y;
      },
    },
    actions: {
      setCurrentRoute({ commit }, y) {
          console.log("state route", y);
        commit('setCurrentRoute', { y });
      },
    }
}