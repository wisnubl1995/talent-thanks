export default{
    namespaced: true,
    state:{
        currentSound: "",
        currentLoadedState: false,
        currentPauseState: false,
        flushSounds: false,
    },
    getters:{
        currentSound: (state) => state.currentSound,
        currentLoadedState: (state) => state.currentLoadedState,
        currentPauseState: (state) => state.currentPauseState,
        flushSounds: (state) => state.flushSounds,
    },
    mutations: {
        setCurrentSound(state, {y}){
            state.currentSound = y
        },
        setCurrentLoadedState(state, y){
            console.log("received stored sound mutations", y);
            state.currentLoadedState = y
        },
        setCurrentPauseState(s, y){
            console.log(y);
            s.currentPauseState = y
        },
        setFlushSounds(s, y){
            console.log(y);
            s.flushSounds = y
        }
    },
    actions:{
        setCurrentSound({commit}, y){
            console.log("received stored sound", y);
            commit('setCurrentSound', {y});
        },
        setCurrentLoadedState({commit}, y){
            console.log("received stored loaded state",y);
            commit('setCurrentLoadedState', y)
        },
        setCurrentPauseState({commit}, y){
            console.log("currentPauseState stored: ", y);
            commit('setCurrentPauseState', y)
        },
        setFlushSounds({commit}, y){
            console.log("flushed sound stored: ", y);
            commit('setFlushSounds', y)
        }
    }
}