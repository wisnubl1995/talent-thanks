import Vue from 'vue'
import Vuex from 'vuex'
import currentroute from './currentroute'
import currentSound from './currentsound'
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    route: currentroute,
    sound: currentSound
  },
  state: {
    currentScrollY: 0,
  },
  getters: {
    currentScrollY: s => s.currentScrollY
  },
  mutations: {
    setCurrentScrollY (s, {y}) {
       s.currentScrollY = y;
    },
  },
  actions: {
    setCurrentScrollY({ commit }, y) {
      commit('setCurrentScrollY', { y });
    },
  },
})
